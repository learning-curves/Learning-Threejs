# Study Notes for Book:
### [Learning Three.js: The JavaScript 3D Library for WebGL - Second Edition](https://www.packtpub.com/web-development/learning-threejs-%E2%80%93-javascript-3d-library-webgl-second-edition) by [Jos Dirksen](http://www.smartjava.org/content/my-books)

---

## Table of Contents
#### Chapters

1. [Creating Your First 3D Scene with Three.js](#chapter-1-creating-your-first-2d-scene-with-threejs)
2. [Basic Components That Make Up a Three.js Scene](#chapter-2-basic-components-that-make-up-a-threejs-scene)
3. [Working with the Different Light Sources Available](#working-with-the-different-light-sources-available)
4. Working with Three.js Materials
5. Learning to Work with Geometries
6. Advanced Geometries and Binary Operations
7. Particles, Sprites, and the Point Cloud
8. Creating and Loading Advanced Meshes
9. Animations and Moving the Camera
10. Loading and Working with Textures
11. Custom Shaders and Render Postprocessing
12. Adding Physics and Sounds to Your Scene

#### Miscellaneous
- [Found Resource Stew](#found-resource-stew) _(bookmarks, resources, tools, etc I've found along the way)_

---

## Chapter 1: Creating Your First 2D Scene with Three.js

- Scene
- Camera
- Renderer
- Add objects
  - `scene.add()` + positioning
- camera.position *ing*
- Append to DOM `renderer.domElement`
- Render
  - `renderer.render(scene, camera)`
- MeshBasicMaterial doesn't reflect light
  - MeshLambertMaterial, MeshPhongMaterial do reflect light
- SpotLight = `THREE.SpotLight(0xffffff)` *(hex number preferred as oppoed to hex string, e.g. '#ffffff')*
  - Shadows
    - `renderer.shadowMapEnabled = true` to enable shadows
    - `.castShadow = true` to objects that should cast a shadow
    - `object.receiveShadow = true` to those that should have shadows cast upon them
- `requestAnimationFrame()`
  - Avoids `setInterval()` which isn't performant
  - Defers frame management to browser
  - Self-referential, e.g.

    ```javascript
    function renderScene() { // <-----------------
      //                                          |
      requestAnimationFrame(renderScene); // <----
      renderer.render(scene, camera);
    }
    ```

- `dat.GUI`
  - ![dat.gui.png](a/i/dat.gui.png)
  - Simple api for adding variables for dat.GUI to watch and update in realtime
  - [GitHub repo](https://github.com/dataarts/dat.gui)
  - [Chrome Examples](https://workshop.chromeexperiments.com/examples/gui/#1--Basic-Usage)

    ```javascript
    gui.add(controls, 'rotationSpeed', 0, 0.5);
    ```
- `onResize()` - Altered the book's code w/ a rudimentary debouncer: [[SO](http://stackoverflow.com/a/5490021/2480125)]

    ```javascript
    var resizeTimeout;
    window.onresize = function() {
      clearTimeout(resizeTimeout);
      resizeTimeout = setTimeout(onResize, 70); // debounce 70 milliseconds
    };
    ```

### Chapter 1 Complete

![](a/v/unbelievable-3d.webm)

---

## Chapter 2: Basic Components That Make Up a Three.js Scene

- Lights
- Camera
- Objects
- `THREE.Scene` is a _"Scene Graph"_
  - Consists of nodes in a tree
  - Root node is `THREE.Object3D`
- `scene.children(.length)`
- `scene.getObjectByName(name)`
- `scene.traverse()` _(recursive)_
- fog / distance visibility
  - `scene.fog = new THREE.Fog(...)`
  - `scene.fog = new THREE.FogExp2(...)`
- `scene.overrideMaterial` _(all objects change material)_
- Geometries are made up of:
  - vertices _(plural)_
  - vertex _(singular)_
  - faces _(connecting vertices)_
    - faces consist of three vertices _(triangle)_
    - Quad faces are used for modeling for greater detail and smoothing
    - Triangles are better for game engines and real-time rendering _(like Three.js)_
  - You could also define a custom geometry w/ `THREE.Geometry()` and providing your own vertices and faces
  - `computeFaceNormals()` used to compute lighting effects on faces
- THREE expects geometries not to change, so in order to change them in real-time, within the render loop you need to loop through the scene:

```javascript
mesh.children.forEach(function(e) {
    e.geometry.vertices=vertices;
    e.geometry.verticesNeedUpdate=true;
    e.geometry.computeFaceNormals();
});
```

- geometry.clone - creates another geometry
- Multi-material objects / meshes: `THREE.SceneUtils.createMultiMaterialObject(geom, materials);`
  - Puts multiple meshes into a group as `children` of an `Object3D`
  - same features as scene, e.g.
  - `mesh.children`
  - `children[0].translateX/Y/Z`
  - `mesh.children[0].translateX(5)`
  - `mesh.traverse(e => e instanceof THREE.Mesh && console.log(e))` (_traverse_ returns the group container Object3D itself, so need to distinguish between prototypes. Also, `traverse` is recursive, so `mesh.children.forEach(e => console.log(e))` while be better if you want to limit the callback's traversal to immediate children of the array being iterated w/out parent or children)
  - etc

### Experimenting

> See fiddle: https://jsfiddle.net/smonsen/jLxbz8n1/

- Click `splitMultiMesh` control to separate wireframe from material mesh

```javascript
gui.add(new function () {
    this.splitMultiMesh = () => {
        var meshes = mesh.children;
        console.log('meshes:', meshes);
        meshes[0].translateX(-5).translateZ(-5);
    }
}, 'splitMultiMesh');
```

- `THREE.WireframeHelper()` -

- **Select:** Used [this SO](http://stackoverflow.com/a/32956439/2480125) w/ [this fiddle](https://jsfiddle.net/yLfrrkfx/2/) to select the multi-mesh object
- **Sound:** Then [this sound demo](https://threejs.org/examples/misc_sound.html) from https://threejs.org/examples/ to add a frequency tone that changes when object is clicked.
- **Mute:** Use the `muteSound` control in the controls panel.
- Combined with the raycaster's returned `intersects` to only change freq when a Mesh is clicked. (`intersects[ 0 ].object instanceof THREE.Mesh`)
- _**Note:**_ Had to use r74 of THREE.js to use `raycaster.setFromCamera()` method. Book uses r69.

---

- Mesh Manipulation
  - `position`
    - relative to parent
    - .set(x,y,z) or individually as properties, e.g. _(mesh.position.x)_
    - mesh.position = new THREE.Vector3(x,y,z)
  - `rotation`
    - same methods as above, but w/ `pi` e.g. _0.5 * Math.PI_
    - to use degrees:

    ```javascript
    deg = 45
    radians = deg * (Math.PI * 180)
    ```

  - `scale`
    - less than one = shrink, greater = opposite
  - `translateX/Y/Z`
    - move object in x,y,z direction relative to it's current position
  - `visible` _self explanitory.._
    - I'm assuming that, unlike css, `visible` removes the onject from affecting the scene, like `display: none` in css. To make something invisible, but still affect the 3D space you would probably change the opacity of the mesh(es) to 0. We'll see.
  - More in Ch. 5 & 7

#### Orthographic and Perspective Cameras

- THREE.PerspectiveCamera(fov, aspect, near, far, zoom)
  - `fov` is usually 60 - 90 deg _(good default is 50)_
  - `aspect` ratio is window height / width
  - `near` & `far` how close and far away to render scene _(defaults: 0.1, 1000 respectively)_
  - `zoom` default: 1
- THREE.OrthographicCamera(left, right, top, bottom, near, far, zoom)
  - defines a cuboid area to render

#### Camera.lookAt

- camera.lookAt(**scene**.position)
- camera.lookAt(**mesh**.position)
- `new THREE.Vector3(x,y,z)` creates a new `THREE.Vector3` object with x,y,z properties available.

### Chapter 2 Complete

![](a/v/ch2-ending.webm)
Fiddle: https://jsfiddle.net/smonsen/nejeu3tt/

---

## Chapter 3: Working with the Different Light Sources Available

- A prerequisite for working w/ materials in next chapter
- WebGL doesn't have native support for lighting
- THREE.Light is parent class


- THREE.AmbientLight
  - comes from all directions (hence _ambient_)
  - **no shadows**
  - adds colors to objects w/ it's color
  - used in conjunction w/ other lighs to soften shadows and/or add color


- THREE.Color
  - Used to change the color of lights and material
  - Properties
    - set(_color_) - _hex / string / THREE.Color_
    - setHex
    - setRGB
    - copy
    - clone
    - (...)


- THREE.PointLight
  - **no shadows**
  - Light emanates from central point in all directions
  - Properties:
    - color - _THREE.Color_
    - distance / 0 = _distance not calculated_, positive whole integers from there
    - intensity / 1
    - position / Vector3, e.g. `position.set(Vector3(x,y,z))`
    - visible
  - Remember, the light source is invisible. So you can only see it affect other objects


- THREE.SpotLight
  - casts shadows
  - cone-like shape like a flashlight, lantern, etc
  - Properties:
    - angle - width of light beam / _def: Math.PI / 3_
    - castShadow
    - color
    - distance - to which light shines / 0
    - exponent - _number_ representing the rate at which the light diminishes in intensity at distance
    - intensity / 1
    - onlyShadow - no light, just shadow casting
    - position - THREE.Vector3
    - shadowBias - moves shadows being cast away or toward / 0
    - shadowCameraFar - how far from light should shadows be cast / 5000
    - shadowCameraNear - same as above, but near / 50
    - shadowCameraFov - field of view from light that should cast shadows / 50
    - shadowCameraVisible / false
    - shadowDarkness - can't be changed / 0.5
    - shadowMapHeight/Width - pixel density of shadow / 512
    - target - requires an Object3D like a Mesh; not Vector3
    - visible
  - Aim `target` at the position of an Object3D _e.g. target.position = Vector3_, then _spotlight.target = target_
  - THREE.shadowMapType = THREE.PCFShadowMap _(default)_ / PCFSoftShadowMap


- THREE.DirectionalLight


_(...)_
















---

## Found Resource Stew
- Chrome Extension: [Three.js Inspector](https://chrome.google.com/webstore/detail/threejs-inspector/dnhjfclbfhcbcdfpjaeacomhbdfjbebi) from: [learningthreejs.com](http://learningthreejs.com/)
- http://www.zamzar.com/convert/mp4-to-webm/
- [dat.guiVR](https://workshop.chromeexperiments.com/examples/guiVR/#1--Basic-Usage) <--- `@todo` _Explore. This. A.S.A.P._
- https://webvr.info/
- https://threejs.org/examples/misc_sound.html _(sound demo)_
